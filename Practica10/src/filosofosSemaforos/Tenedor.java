package filosofosSemaforos;

public class Tenedor {
	
	EdoFilosofo<String> estado_f = new EdoFilosofo<String>();
	SemaforoBinario mutex = new SemaforoBinario(true);
	SemaforoContador [] semaforos = new SemaforoContador[5];
	
	public Tenedor(){
		for(int i=0; i < semaforos.length; i++){
			semaforos[i] = new SemaforoContador(0);
			estado_f.add("PENSAR");
		}
	}
	
	public void tomar_tenedores(int i){
		mutex.P();
		estado_f.set(i, "HAMBRE");
		probar_bocado(i);
		mutex.V();
		semaforos[i].P();
		
	}
	
	public void probar_bocado(int i){
		if((estado_f.get(i) == "HAMBRE") && (estado_f.get(i-1) != "COMER") && (estado_f.get(i+1) != "COMER")){
			estado_f.set(i, "COMER");
			semaforos[i].V();
		}
	}
	
	public void dejar_tenedores(int i){
		mutex.P(); /*Inicia la CR */
		probar_bocado(i-1); /* Deja comer al fil�sofo de la
							izquierda, si es posible*/
		probar_bocado(i+1); /* Deja comer al fil�sofo de la 
							derecha, si es posible*/
		estado_f.set(i, "PENSAR"); /* Cambia al fil�sofo actual
									en estado: PENSAR*/
		mutex.V(); /*Se libera la CR */
	}
}
