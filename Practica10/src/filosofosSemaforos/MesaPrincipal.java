package filosofosSemaforos;

import filosofosSemaforos.Filosofo;

public class MesaPrincipal {
	public static void main(String args[]){
		//Instancia para el Tenedor
		Tenedor tenedor 			= new Tenedor();
		//Arreglo para tener 5 instancias de la clase Filosofo
		Filosofo[] num_filosofos 	= new Filosofo[5];
		//Se crean las 5 instancias de Filosofo
		for(int i=0; i<num_filosofos.length; i++){
			num_filosofos[i] = new Filosofo(i, tenedor);
		}
		//Se ejecuta
		for(int i=0; i<num_filosofos.length; i++){
			num_filosofos[i].start();
		}
	}

}
