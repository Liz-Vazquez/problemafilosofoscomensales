package filosofosMonitores;

public class Filosofo extends Thread{
	int id;
	Tenedor tenedor = new Tenedor();
	
	public Filosofo(int id, Tenedor tenedor){
		this.id 	 = id;
		this.tenedor = tenedor;
	}
	
	public void pensando(){
		System.out.println("El fil�sofo "+(id+1)+" est� pensando...");
	}
	
	public void comiendo(){
		System.out.println("El fil�sofo "+(id+1)+" est� comiendo con tenedor "+(id+1)+" y tenedor "+(((id+1)%5)+1));
	}
	
	public void terminar_comida(){
		System.out.println("El fil�sofo "+(id+1)+" deja los tenedores "+(id+1)+" y "+(((id+1)%5)+1));
	}
	@Override
	public void run() {
		while(true){
			pensando();
			tenedor.tomar_tenedores(id);
			Util.mySleep(500);
			comiendo();
			Util.mySleep(500);
			tenedor.dejar_tenedores(id);
			terminar_comida();
			Util.mySleep(500);
		}
	}

}
