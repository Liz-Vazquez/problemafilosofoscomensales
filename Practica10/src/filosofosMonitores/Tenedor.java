package filosofosMonitores;

public class Tenedor {
	
	EdoFilosofo<String> estado_f = new EdoFilosofo<String>();
	
	public Tenedor(){
		for(int i=0; i < 5; i++){
			estado_f.add("PENSAR");
		}
	}
	
	public synchronized void tomar_tenedores(int i){ //M�todo de ingreso
		while(estado_f.get(i-1) == "COMER" || estado_f.get(i+1) == "COMER")/* Condici�n para la sincronizaci�n,
																		mientras los tenedores est�n ocupados*/
			
			Util.myWait(this);/*bloquea y coloca el hilo dentro de la cola de procesos,
								variable de condici�n */
		
		estado_f.set(i, "HAMBRE");
		probar_bocado(i);		
	}
	
	public  void probar_bocado(int i){
		if((estado_f.get(i) == "HAMBRE") && (estado_f.get(i-1) != "COMER") && (estado_f.get(i+1) != "COMER")){
			estado_f.set(i, "COMER");
		}
	}
	
	public synchronized void dejar_tenedores(int i){ //M�todo de ingreso
		probar_bocado(i-1);/* Deja comer al fil�sofo de la
							izquierda, si es posible*/
		probar_bocado(i+1);/* Deja comer al fil�sofo de la 
							derecha, si es posible*/	
		estado_f.set(i, "PENSAR"); /* Cambia al fil�sofo actual
									en estado: PENSAR*/
		notify();/* notifica a la cola de procesos bloqueados,
		variable de condici�n*/
	}

}
