package filosofosMonitores;
import java.util.ArrayList;

//Clase para la lista circula y manejar los estados de los filos�fos
public class EdoFilosofo<edo> extends ArrayList<edo>{
	
    private static final long serialVersionUID = 1L;

    public edo get(int index){
        if (index < 0)
            index = size()-1;
        else if (index >= size())
            index = 0;

        return super.get(index);
    }
    
    public edo set(int index, edo element){
    	if (index < 0)
            index = size()-1;
        else if (index >= size())
            index = 0;
    	
		return super.set(index, element);
    }
    
}